from setuptools import setup

setup(
    name='OTils',
    version='0.1.0',
    author='Oriol Tintó Prims',
    author_email='oriol.tinto@lmu.de',
    packages=['OTils'],
    # scripts=['bin/script1','bin/script2'],
    # url='http://pypi.python.org/pypi/PackageName/',
    license='LICENSE.txt',
    description='Some utilities to make my life easier',
    long_description=open('README.md').read(),
    install_requires=[
        "numpy",
        "omegaconf",
        "psutil",
    ],
    entry_points={
        'console_scripts': [
            'give_me_a_data_file=OTils.file_dealer:give_me_a_file',
        ],
    },

    scripts=['bin/slurm_queue_permissions'],

    package_data={'': ['conf/configuration.yaml']},
    include_package_data=True,
)

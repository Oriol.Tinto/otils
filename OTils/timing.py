import time


class Timer:
    def __init__(self, name=None):
        self.name = name

    def __enter__(self):
        self.start_time = time.perf_counter()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.end_time = time.perf_counter()
        prefix = f"Elapsed time in {self.name!r}:" if self.name is not None else "Elapsed time:"

        print(f"{prefix} {self.end_time - self.start_time:.2f}", flush=True)


# Context manager to measure the time it takes to execute a block of code
def time_it(func):
    def wrapper(*args, **kwargs):
        with Timer(name=func.__name__):
            return func(*args, **kwargs)

    return wrapper


if __name__ == "__main__":
    @time_it
    def do_something():
        time.sleep(1)
        print("Doing something")


    do_something()

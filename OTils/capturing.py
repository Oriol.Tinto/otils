# Functions to capture stdout or stderr produced by a python call.
# Can both be unified?

import sys
from io import StringIO


class Capturing(dict):
    """
        Class to capture output of function.
        Usage:

            with Capturing() as output:  # note the constructor argument
                print('hello world2')

            print('output:', output)
        By default it only captures stdout, can capture also stderr by using the argument capture_stderr=True
            with Capturing(capture_stderr=True) as output:  # note the constructor argument
                print('hello stderr', file=sys.stderr

            print('error:', output)
        """
    def __init__(self, capture_stdout=True, capture_stderr=False):
        super().__init__()
        self.capture_stdout = capture_stdout
        self.capture_stderr = capture_stderr
        self.stdout = None
        self.stderr = None

    def __enter__(self):
        self._stringio_out = StringIO()
        self._stringio_err = StringIO()
        if self.capture_stderr:
            self._stderr = sys.stderr
            sys.stderr = self._stringio_err
        if self.capture_stdout:
            self._stdout = sys.stdout
            sys.stdout = self._stringio_out
        return self

    def __exit__(self, *args):
        # del self._stringio  # free up some memory
        if self.capture_stdout:
            sys.stdout = self._stdout
            self["out"] = self._stringio_out.getvalue().splitlines()
            del self._stringio_out

        if self.capture_stderr:
            sys.stderr = self._stderr
            self["err"] = self._stringio_err.getvalue().splitlines()
            del self._stringio_err



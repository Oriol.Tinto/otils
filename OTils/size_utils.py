import math
from pathlib import Path
import re

units = {"B": 1, "KB": 1024, "MB": 1024**2, "GB": 1024**3, "TB": 1024**4}


def file_size(file_path):
    return Path(file_path).stat().st_size


def file_list_size(file_list):
    return sum([file_size(fp) for fp in file_list])


def convert_size(size_bytes):
    """
    This function converts the given size in bytes to a more readable format
    (e.g. bytes, kilobytes, megabytes, etc.)
    
    Args:
    size_bytes (int): The size in bytes to be converted
    
    Returns:
    str: The converted size with its unit of measurement (e.g. '5MB')
    """
    import math
    if size_bytes < 0:
        prefix = "-"
        size_bytes = -size_bytes # convert to positive if it's negative
    else:
        prefix = ""

    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024))) # get the power of 1024 based on the size of bytes
    p = math.pow(1024, i) # get the value of the power of 1024
    s = round(size_bytes / p, 2) # divide bytes by the value of power of 1024 and round it to 2 decimal places
    return f"{prefix}{s}{size_name[i]}" # return the size in the format of "5MB"


def convert_to_bytes(size_string):
    """
    This function converts a given size string (e.g. '5MB') to the number of bytes.
    
    Args:
    size_string (str): The size string to be converted (e.g. '5MB')
    
    Returns:
    int: The number of bytes.
    """
    import re
    size_string = size_string.upper()
    digits = re.match(r'\d+(?:\.\d+)?', size_string) # matches digits and optionally a dot followed by more digits
    if digits:
        digits = digits.group() # get the matched digits
    else:
        raise ValueError(f"Invalid size string: {size_string}")
    unit = size_string.replace(digits, "")
    size_name_dict = {'B': 0, 'KB': 1, 'MB': 2, 'GB': 3, 'TB': 4, 'PB': 5, 'EB': 6, 'ZB': 7, 'YB': 8}
    if unit in size_name_dict:
        size_bytes = float(digits) * math.pow(1024, size_name_dict[unit])
    else:
        raise ValueError(f"Invalid size string: {size_string}")
    return int(size_bytes)


def parse_size(size):
    pattern = "([0-9\.]+) *([BKMGT]+)"
    m = re.search(pattern, size)
    if m:
        number = float(m.group(1))
        unit = m.group(2)
    else:
        raise AssertionError(f"Can't parse:{size}")
    return int(float(number) * units[unit])


def compression_ratio(old: str, new: str) -> float:
    return file_size(old) / file_size(new)


if __name__ == "__main__":
    print(convert_to_bytes("10PB"))


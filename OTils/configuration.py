# To avoid reinventing the wheel I'll just use omegaconf as it seems like a reliable library.
from omegaconf import OmegaConf
from os.path import dirname, join
# Path to the configuration file
configuration_file = "conf/configuration.yaml"


# Initialize configuration
# Make some tricks to point to the proper folder
dirname = dirname(__file__)
configuration_path = join(dirname, configuration_file)

configuration = OmegaConf.load(configuration_path)



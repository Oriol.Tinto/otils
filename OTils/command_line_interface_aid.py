from typing import Callable, List


class CommandLineInterface:
    """
    This class pretends to be a facilitator to create command line interfaces.
    """

    def __init__(self):
        import argparse
        self.parser = argparse.ArgumentParser()
        self.subscribers = {}

    def add_action(self, name: str, aliases: List[str], action: Callable, type=None, **kwargs):
        """

        Args:
            name: Name of the action
            aliases: Aliases that will be usable
            action: function that will be called if the command line options is called.
            type: type of the argument expected. If None, a bool will be used.
            **kwargs: The kwargs will be directly passed to the parser.add_argument method.

        Returns:
            None
        """
        if type is None:
            self.parser.add_argument(*aliases, dest=name, action='store_true', **kwargs)
        else:
            self.parser.add_argument(*aliases, dest=name, type=type, **kwargs)
        self.subscribers[name] = action

    def parse(self, args=None):
        """
        Parse arguments and trigger
        Args:
            args: Command line arguments. Using stdin if none provided to the call.

        Returns:
            parsed arguments object.
        """
        args = self.parser.parse_args(args)
        self.run_subscribers(args)
        return args

    def run_subscribers(self, args) -> None:
        """
        Goes through the list of arguments and executes the corresponding actoins.
        Args:
            args:

        Returns:
            None
        """
        for name in self.subscribers.keys():
            param = getattr(args, name)
            if param:
                self.subscribers[name](param)

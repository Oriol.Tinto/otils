from OTils.size_utils import convert_size


class MemoryTracker:
    def __init__(self):
        import psutil
        import os
        self.proc = psutil.Process(os.getpid())
        self._start = None
        self.start()

    def memory(self):
        return self.proc.memory_info().rss

    def readable(self):
        return convert_size(self.memory())

    def start(self):
        self._start = self.memory()

    @property
    def difference(self):
        return self.memory() - self._start

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __repr__(self):
        return f"MemoryTracker(Total: {self.readable()} Increment: {convert_size(self.difference)})"


from os import listdir
from os.path import isdir
from typing import List
from OTils.size_utils import parse_size, file_size
from OTils.configuration import configuration


def file_dealer_is_available():
    """
    Check that file_dealer is properly configured.
    Returns:

    """
    files_folder = configuration.folders.data
    return isdir(files_folder)


def get_list_of_files_of_approximate_size(list_of_files, total_size):
    size_in_bytes = parse_size(total_size)
    accumulated_size = 0
    error = abs(size_in_bytes - accumulated_size)
    length = 0

    for file_path in list_of_files:
        f_size = file_size(file_path)
        temporary_accumulated_size = accumulated_size + f_size
        temporary_error = abs(size_in_bytes - temporary_accumulated_size)
        if temporary_error < error:
            accumulated_size = temporary_accumulated_size
            error = temporary_error
            length += 1
        else:
            break

    selected_files = list_of_files[:length]
    return selected_files


def file_dealer(number_of_files=None, total_size=None) -> List[str]:
    """
    Small function to get some input filenames fast.

    We can get a list of a certain length using the argument number_of_files or files that add up to a certain size (in GB).
    :filter: Should be ML or PL.
    :rtype: list
    """

    # Some files from the folder specified in the configuration file: "conf/configuration.yaml"
    files_folder = configuration.folders.data
    if not file_dealer_is_available():
        raise IOError(f"Folder {files_folder} provided in the configuration file does not exist.")
    filename_filter = configuration.parameters.filter

    file_names = listdir(files_folder)
    file_names = list(sorted(file_names))

    if filename_filter is not None:
        filtered_files = [f for f in file_names if f.count(filename_filter)]
    else:
        filtered_files = file_names
    file_names = filtered_files
    file_paths = [f"{files_folder}/{fn}" for fn in file_names]

    if number_of_files is not None:
        if number_of_files > len(file_paths):
            raise AssertionError(f"You requested {number_of_files} files which is more than the number of files available:"
                  f"{len(file_paths)}")
        return file_paths[:number_of_files]
    elif total_size is not None:
        return get_list_of_files_of_approximate_size(file_paths, total_size)

    else:
        raise AssertionError("At least one argument is required:\n number_of_files or total_size")


def give_me_a_dataset():
    from enstools.io import read
    file_path = file_dealer(number_of_files=1)[0]
    return read(file_path)


def give_me_data():
    ds = give_me_a_dataset()
    variables = [v for v in ds.variables if v not in ds.coords]
    var = variables[-1]
    return ds[var][0].values


def give_me_a_file():
    from pathlib import Path
    import shutil
    file_path = Path(file_dealer(number_of_files=1)[0])
    current_folder = Path().cwd()
    new_file = current_folder / file_path.name
    shutil.copy(file_path, new_file)


if __name__ == "__main__":
    files = file_dealer(total_size="5GB")
    print(files)

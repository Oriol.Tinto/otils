from typing import List


def suggester(options: List[str], prior: str):
    """
    The purpose of this function is to suggest possible matches when there's no exact match.
    Args:
        options: list of options
        prior:

    Returns:

    """
    from difflib import SequenceMatcher

    def similar(a, b):
        return SequenceMatcher(None, a, b).ratio()

    scores = {o: similar(o, prior) for o in options}

    highest_score = 0
    most_probable_answer = None
    for name, score in scores.items():
        if score > highest_score:
            most_probable_answer = name
            highest_score = score
    # In case the score is really low just return None
    if highest_score < .2:
        return None

    # Otherwise return the most probable answer
    return most_probable_answer

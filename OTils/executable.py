import logging
from dataclasses import dataclass
from pathlib import Path
import subprocess
from typing import List, Union


@dataclass
class Answer:
    return_code: int
    stdout: str
    stderr: str


class BinaryNotFound(Exception):
    ...


def launch_command(command: List[str]) -> Answer:
    """
    Launch a given command in bash and return an Answer object which includes the return code,
    the stdout and the stderr.
    :param command:
    :return:
    """
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    return Answer(process.returncode, stdout.decode(), stderr.decode())


class Executable:
    """
    Class to wrap a system executable and easily launch commands with it.
    """
    def __init__(self, binary_path: Union[Path, str]):
        if not isinstance(binary_path, Path):
            binary_path = Path(binary_path)
        self.name = binary_path.name
        self.binary_path = binary_path

    def __call__(self, *args) -> Answer:
        """
        Calls the binary with the provided arguments and returns an Answer object
        :param args:
        :return:
        """
        command = [self.binary_path, *args]
        answer = launch_command(command)
        if answer.stderr.strip():
            logging.warning(answer.stderr)
        return answer


def which(binary_name: str) -> Executable:
    """
    Uses the system which command to find the path of a certain executable and then returns an Executable object.
    :param binary_name:
    :return:
    """
    command = ["which", binary_name]
    answer = launch_command(command)
    if answer.return_code:
        raise BinaryNotFound(f"Binary {binary_name!r} not found.")
    if answer.stderr.strip():
        logging.warning(answer.stderr)
    binary_path = Path(answer.stdout.strip())
    logging.debug(f"{binary_path=}")
    return Executable(binary_path)


def try_it():
    ls = which("what")
    if ls:
        print(ls("--help"))
    else:
        print("Command not found")


if __name__ == "__main__":
    try_it()

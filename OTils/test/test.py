import pytest


class TestCapturing:
    def test_capturing_stdout(self):
        from OTils.capturing import Capturing
        message = "This is going to be captured"
        with Capturing() as capture:
            print(message)

        captured_message = capture["out"][0]
        assert captured_message == message

    def test_capturing_stderr(self):
        import sys
        from OTils.capturing import Capturing
        message = "This is going to be captured"
        with Capturing(capture_stderr=True) as capture:
            print(message, file=sys.stderr)

        captured_message = capture["err"][0]
        assert captured_message == message


class TestSizeUtils:
    def test_file_size(self):
        from OTils.size_utils import file_size
        script_size = file_size(__file__)
        assert script_size > 0

    def test_file_list_size(self):
        from OTils.size_utils import file_list_size
        files = [__file__, __file__]
        size = file_list_size(files)
        assert size > 0

    def test_convert_size(self):
        from OTils.size_utils import convert_size
        pairs = [("1.0KB", 1024),
                 ("1.0MB", 1024 ** 2),
                 ("1.0GB", 1024 ** 3),
                 ("-1.0GB", -1024 ** 3),
                 ("0B", 0),
                 ]

        for string, value in pairs:
            assert string == convert_size(value)

    def test_parse_size(self):
        from OTils.size_utils import parse_size
        assert 1024 == parse_size("1.0KB")
        with pytest.raises(AssertionError):
            parse_size("patata")


class TestFileDealer:
    def test_file_dealer_number_of_files(self):
        from OTils.file_dealer import file_dealer
        files = file_dealer(number_of_files=2)
        assert len(files) == 2

    def test_file_dealer_total_size(self):
        from OTils.file_dealer import file_dealer
        files = file_dealer(total_size="500MB")
        assert len(files) > 0

    def test_file_dealer_no_arg(self):
        from OTils.file_dealer import file_dealer
        with pytest.raises(AssertionError):
            file_dealer()

    def test_file_dealer_folder_test(self):
        from OTils.configuration import configuration
        # Ugly thing to do
        valid_path = configuration.folders.data
        configuration.folders.data = "None"

        from OTils.file_dealer import file_dealer
        with pytest.raises(IOError):
            file_dealer(number_of_files=1)
        # Restore proper path
        configuration.folders.data = valid_path

    def test_give_me_a_dataset(self):
        from OTils.configuration import configuration
        assert configuration.folders.data != "None"
        from OTils.file_dealer import give_me_a_dataset
        import xarray
        ds = give_me_a_dataset()
        assert isinstance(ds, xarray.Dataset)

    def test_give_me_data(self):
        from OTils.file_dealer import give_me_data
        import numpy
        data = give_me_data()
        assert isinstance(data, numpy.ndarray)


class TestExecutable:
    def test_which(self):
        from OTils.executable import which
        ls = which("ls")
        ls("/")

    def test_wrong_binary(self):
        from OTils.executable import which, BinaryNotFound
        with pytest.raises(BinaryNotFound):
            which("Non_existing_binary")

    def test_executable(self):
        from OTils.executable import Executable
        from pathlib import Path
        bash_path = Path("/bin/bash")
        bash = Executable(bash_path)
        bash("ls")

    def test_executable_from_string(self):
        from OTils.executable import Executable
        bash = Executable("/bin/bash")
        bash("ls")



"""
Few wrappers to easily launch bash commands from python
"""

import asyncio
import subprocess
from typing import Tuple, List


async def async_launch_command(command: str) -> Tuple[bytes, bytes]:
    """
    Launch a command asynchronously.
    Args:
        command:

    Returns:

    """
    proc = await asyncio.create_subprocess_shell(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                                 executable='/bin/bash')
    stdout, stderr = await proc.communicate()
    return stdout, stderr


def launch_command(command: str) -> Tuple[bytes, bytes]:
    """
    Wrapper to launch a single command from outside an async function.
    Args:
        command:

    Returns:

    """
    return asyncio.run(async_launch_command(command))


async def async_launch_commands_in_parallel(commands: List[str]) -> List[Tuple[bytes, bytes]]:
    """
    Asynchronous launch multiple commands in parallel. To be
    Args:
        commands:

    Returns:
        list of stdout and stderr
    """
    jobs = [async_launch_command(command) for command in commands]
    results = await asyncio.gather(*jobs)
    return results  # type: ignore


def launch_commands_in_parallel(commands: List[str]) -> List[Tuple[bytes, bytes]]:
    """
    Synchronous wrapper to launch multiple commands in parallel. To be used outside a async loop.

    Args:
        commands:

    Returns:

    """
    return asyncio.run(async_launch_commands_in_parallel(commands))

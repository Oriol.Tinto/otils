import os
import contextlib
from pathlib import Path
import shutil
import tempfile


@contextlib.contextmanager
def working_directory(path):
    """Changes working directory and returns to previous on exit."""
    prev_cwd = Path.cwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_cwd)


@contextlib.contextmanager
def temporary_directory(prefix="tmp"):
    """
    # Usage example:
    with temporary_directory() as temp_dir:
        print(f"Created temporary directory: {temp_dir}")
        # Do something with the temporary directory
    Args:
        prefix:

    Returns:

    """
    temp_dir = tempfile.mkdtemp(prefix=prefix)
    try:
        yield temp_dir
    finally:
        shutil.rmtree(temp_dir)


@contextlib.contextmanager
def temporary_working_directory(prefix="tmp"):
    temp_dir = tempfile.mkdtemp(prefix=prefix)
    prev_cwd = Path.cwd()
    os.chdir(temp_dir)
    try:
        yield temp_dir
    finally:
        os.chdir(prev_cwd)
        shutil.rmtree(temp_dir)
